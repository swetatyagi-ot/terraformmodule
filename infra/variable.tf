########## env name##############
variable "env" {
  description = "env to deploy into, should typically ab_dev/ab_qa/ab_prod"
  default     = "dev"
}

###### Variable for Region ##########
variable "infra_region" {
  description = "region for the infra"
  default     = "us-west-2"
}

variable "key_credentials_path" {
  description = "key credentials path"
  default     = ""
}

###################### VPC vars #####################
# VPC CIDR block
variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  default     = "10.0.0.0/16"
}

# VPC Tenancy
variable "instanceTenancy" {
  default = "default"
}

# VPC dns support value
variable "dnsSupport" {
  default = true
}

# VPC dns host name value
variable "dnsHostNames" {
  default = true
}

###########Tag for VPC
variable "vpc_tag_name" {
  description = "Tag for VPC Name fields"
  default     = "vpc"
}

###########variables for first public subnet########
#### CIDR Block
variable "cidr_pub_subnet_az_a" {
  description = "The CIDR block for the subnet."
  default     = "10.0.15.0/24"
}

#### Tag Name 
variable "pub_subnet_name_az_a" {
  description = "name of public subnets"
  default     = "pub_subnet_az_a"
}

###########variables for second public subnet########
#### CIDR Block
variable "cidr_pub_subnet_az_b" {
  description = "The CIDR block for the subnet."
  default     = "10.0.16.0/24"
}

#### Tag Name 
variable "pub_subnet_name_az_b" {
  description = "name of public subnets"
  default     = "pub_subnet_az_b"
}

####### Two Availability zone for all subnets#########3
variable "availability_zone_a" {
  description = "The AZ for the subnet"
  default     = "us-west-2a"
}

variable "availability_zone_b" {
  description = "The AZ for the subnet"
  default     = "us-west-2b"
}

