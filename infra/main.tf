################# Region of Infra #################
provider "aws" {
  version                 = "~> 2.0"
  shared_credentials_file = var.key_credentials_path
  region                  = var.infra_region
}

######## Creating VPC ####################
module "vpc" {
  source               = "../vpc"
  cidr_block           = var.vpc_cidr_block
  instance_tenancy     = var.instanceTenancy
  enable_dns_support   = var.dnsSupport
  enable_dns_hostnames = var.dnsHostNames
  vpc_Name_tag         = "${var.env}_${var.vpc_tag_name}"
}

#########Creating First Public Subnet ##############
module "bastion_host_pub_subnet_az_a" {
  source            = "../subnets"
  vpc_id            = module.vpc.id
  cidr_subnet       = var.cidr_pub_subnet_az_a
  availability_zone = var.availability_zone_a
  subnet_name       = "${var.env}_${var.pub_subnet_name_az_a}"
}

############Creating Second Public Subnet############
module "bastion_host_pub_subnet_az_b" {
  source            = "../subnets"
  vpc_id            = module.vpc.id
  cidr_subnet       = var.cidr_pub_subnet_az_b
  availability_zone = var.availability_zone_b
  subnet_name       = "${var.env}_${var.pub_subnet_name_az_b}"
}

